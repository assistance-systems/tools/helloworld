/*
    Konventionen zur Namensgebung und Kommentierung im Quelltext
    ============================================================
    * Klassen werden groß geschrieben. Alle Anfangsbuchstaben innerhalb des Klassennamens werden groß geschrieben.
    * Variablennamen werden klein geschrieben.
    * Membervariablen werden klein geschrieben.
    * Funktionen/Methoden haben die gleiche Groß- und Kleinschreibung, wie Variablen.
    * Konstanten bestehen nur aus Großbuchstaben, Wörter in den Konstantennamen werden durch "_" getrennt.
    * Funktionen/Methodennamen enthalten Verben, Klassennamen enthalten Substantive.
    * Kommentare für Funktionen/Methoden, falls überhaupt nötig, enthalten:
    -- Eine allgemeins Beschreibung der Funktion/Methode
    -- Eine Beschreibung jedes einzelnen Parameters
    -- Eine Beschreibung des Rückgabewertes
    -- Der zulässige Wertebereich der Parameter und der Wertebereich des Rückgabewertes
    -- Besonderheiten (z.B. geworfene Exceptions,...)
    * Auch Klassen können Kommentare enthalten, in denen Allgemeines über die Klasse steht.
    * Alle Bezeichner auf Englisch. Alle Kommentare auf Deutsch!
    * Einrückung nach VS Code/Arduino IDE Autoformatieren
    * Variablennamen haben sprechende Namen, die durchaus etwas länger sein können.

    Konventionen zur Programmierung
    ===============================
    * In jede Headerdatei gehört : #ifndef.. #define.. #endif
    * Jede Funktion/Methode soll nur eine Aufgabe übernehmen.
    * Jede Funktion/Methode erhält die von ihr benötigten Daten per Parameter (am besten als Referenz)
    -> damit werden die Funktionen/Methoden besser testbar
    * Jede Funktion/Methode soll mit einem oder mehreren Unittests getestet werden, soweit das möglich ist.
    * Jede Funktion/Methode soll nach Fertigstellung von einem Kollegen einem Review unterzogen werden.

    Konventionen zur Anordnung
    =================================
    * Die Reihenfolge von Includes wird nach dem folgenden Schema durchgeführt:
    -- Eigene Headerdatei
    -- Leerzeile
    -- C System Header
    -- Leerzeile
    -- C++ Standardbibliotheken
    -- Leerzeile
    -- Andere Bibliotheken
    -- Leerzeile
    -- Projektheader
 */


#ifndef CODE_CONVENTIONS_H
#define CODE_CONVENTIONS_H

#define KONST_TO_SHOW

#include "code_conventions.h"

#include <sys/types.h>
#include <unistd.h>

#include <string>
#include <vector>

#include "another_header.h"

class ClassForDemonstration
{
private:
    byte member_example_1;
    byte memberExample2;

public:
    byte get_member_1() { return member_example_1; };
    byte getMember2() { return memberExample2; };
    void set_member_1(byte aValue) { member_example_1 = aValue; };
    void setMember2(byte aValue) { memberExample2 = aValue; };
};

ClassForDemonstration demo_object;

void setup()
{
    Serial.begin(115200);
}

void loop()
{
    fill_object(demo_object);
    serial_out_object(Serial, demo_object);
    Serial.println();
}

void fill_object(ClassForDemonstration &aObject)
{
    aObject.set_member_1(millis());
    aObject.setMember2(millis());
}

void serial_out_object(Stream &aStream, ClassForDemonstration &aObject)
{
    aStream.print(aObject.get_member_1());
    aStream.print("\t");
    aStream.print(aObject.getMember2());
}

#endif
